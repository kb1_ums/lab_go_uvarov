package random

func random(arr []float64, m float64, a float64, c float64, n int, maxRange int) {
	arr[0] = rand.Float64() * float64(maxRange)
	for i := 1; i < n; i++ {
		arr[i] = math.Mod(arr[i-1]*a+c, float64(maxRange))
	}
}
