// main
package main

import "fmt"
import myMath "ex4/math"

func main() {
	fmt.Println("Min value = ", myMath.searchMin(5, 3, 5))
	fmt.Println("Avarage value = ", myMath.avarageValue(5, 3, 5))
}
