package main

import "fmt"

func main() {
	var chartype int8 = 'R'
	fmt.Printf("Code '%c' - %d\n", chartype, chartype)

	var test int32 = 'Ї'
	fmt.Printf("Code '%c' - %d\n", test, test)

	//rune: синоним типа int32, представляет целое число от -2147483648 до 2147483647 и занимает 4 байта

	//Задание.
	//1. Вывести украинскую букву 'Ї'
	//2. Пояснить назначение типа "rune"
}
