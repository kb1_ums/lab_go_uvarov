package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	var arr [11]int
	sum := 0
	product := 1
	for i := 0; i < 11; i++ {
		arr[i] = rand.Intn(10) - 4
		if arr[i] < 0 {
			sum += arr[i]
		}
		product *= arr[i]
	}
	fmt.Println(arr)
	fmt.Println("Sum - ", sum)
	fmt.Println("Product - ", product)
}
