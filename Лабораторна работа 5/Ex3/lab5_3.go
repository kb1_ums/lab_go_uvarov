package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var arr [11]int
	for i := 0; i < 11; i++ {
		arr[i] = rand.Intn(10) - 4
	}
	fmt.Println(arr)
}
