package main

import (
	"fmt"
	"net/http"
	"strconv"
)

const (
	pageHeader = `<!DOCTYPE HTML>
<html>
<head>
<title>Задание 1</title>
<style>
.error{
color:#FF0000;
}
</style>
</head>`
	pageBody = `<body>
<h1>Решение</h1>
<p>Это описание задания</p>`
	form = `<form action="/" method="POST">
<label for="numbers">Введите число:</label><br />
<input type="text" name="numbers" size="20" value="5"><br />
<input type="submit" value="Выполнить">
</form>`
	pageFooter = `</body></html>`
	table      = `<table border="1">
<tr><th colspan="2">Результат</th></tr>
<tr><td>Строка 1</td><td>%s</td></tr>
<tr><td>Строка 2</td><td>%f</td></tr>
</table>`
	anError = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Задание1"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody, form) // Формируем страницу в браузере
	if r.Method == "POST" {                   // Обрабатываем входные данные
		err := r.ParseForm() // Парсим форму
		post := r.PostForm
		if err != nil {
			fmt.Fprintf(w, anError, err)
			return
		}
		pnumbers := post.Get("numbers")
		numbers, _ := strconv.ParseInt(pnumbers, 10, 32)
		fmt.Fprintf(w, "%v", numbers) // Выводим сообщение в браузер
		fmt.Fprintf(w, "\n\t<br /><br />")
		fmt.Fprintf(w, table, "Текст", 5.6)
	}
	fmt.Fprint(w, "\n", pageFooter)
}
func main() {
	// Запускаем локальный сервер
	http.ListenAndServe("localhost:80", HttpSolution1)
}
